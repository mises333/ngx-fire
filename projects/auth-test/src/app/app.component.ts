import { Component } from '@angular/core';
import { AuthService } from 'projects/ngx-fire/auth/src/lib/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'auth-test';

  constructor(public auth: AuthService) {}
}
