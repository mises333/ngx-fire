import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { FirestoreSettingsToken, AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { DeleteAccountComponent } from './user/delete-account/delete-account.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { LoginPanelComponent } from './login/components/login-panel/login-panel.component';
import { SignupPanelComponent } from './login/components/signup-panel/signup-panel.component';
import { SignupProvidersComponent } from './login/components/signup-providers/signup-providers.component';
import { SnackService } from './shared/snack.service';
import { AuthErrService } from './shared/auth-err.service';
import { TranslModule } from './shared/transl.module';
import { NgxFireAuthModule, AuthErrorService } from 'projects/ngx-fire/auth/src/public-api';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    UserComponent,
    HomeComponent,
    DeleteAccountComponent,
    ProgressBarComponent,
    LoginPanelComponent,
    SignupPanelComponent,
    SignupProvidersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgxFireAuthModule.forRoot({ enableFirestoreSync: true, enableAuthErrorService: true }),
    TranslModule,
  ],
  entryComponents: [
    DeleteAccountComponent,
  ],
  providers: [
    AuthErrService,
    SnackService,
    { provide: AuthErrorService, useExisting: AuthErrService },
    { provide: FirestoreSettingsToken, useValue: {} },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
