import { Component, OnInit } from '@angular/core';
import { AuthService, User, AuthSyncService } from 'projects/ngx-fire/auth/src/public-api';
import { SnackService } from '../shared/snack.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private auth: AuthService, private sync: AuthSyncService, private snack: SnackService) { }

  ngOnInit() {

  }

  button(){
    if(this.auth.afAuth.auth.currentUser) {
      try {
        const data: User = {
          uid: this.auth.afAuth.auth.currentUser.uid,
        }
        this.sync.setUserData(data)
        this.snack.open("Update Success!", this.snack.config('success'))
      } catch(err) {
        this.snack.open(err.message)
      }
    }
  }

}
