import { Component, Input } from '@angular/core';
import { SnackService } from '../../../shared/snack.service';
import { AuthService, AuthProvider } from 'projects/ngx-fire/auth/src/public-api';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.css']
})
export class LoginPanelComponent {

  hide: true;
  @Input() disabled: boolean;

  email = new FormControl('', [Validators.required, Validators.email])
  password = new FormControl('', [Validators.minLength(6), Validators.required])
  formIsValid: boolean;

  constructor(private snack: SnackService,
    private auth: AuthService) { }

  async logIn() {
    if (this.email.valid && this.password.valid) {
      try {
        let credent: any = []
        credent.email = this.email.value
        credent.password = this.password.value
        await this.auth.signInWith(AuthProvider.EmailAndPassword, credent)
        this.snack.open('Login success.')
      } catch (err) {
        this.snack.open(err.message)
      }
    } else {
      this.formIsValid = false;
    }
  }

  getErrorPassword() {
    return this.password.hasError('required') ? 'You must enter a password.' : '';
  }

  getErrorEmail() {
    return this.email.hasError('required') ? 'You must enter a email.' :
      this.email.hasError('email') ? 'Not a valid email.' : '';
  }

  getErrorForm() {
    return this.email.hasError('required') ? 'You must enter a email.' :
      this.password.hasError('required') ? 'You must enter a password.' :
        this.email.hasError('email') ? 'Not a valid email.' : '';
  }

}
