import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupProvidersComponent } from './signup-providers.component';

describe('SignupProvidersComponent', () => {
  let component: SignupProvidersComponent;
  let fixture: ComponentFixture<SignupProvidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupProvidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
