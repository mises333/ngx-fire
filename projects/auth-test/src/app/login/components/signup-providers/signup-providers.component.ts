import { Component, OnInit } from '@angular/core';
import { AuthProvider } from 'projects/ngx-fire/auth/src/lib/auth.interfaces';
import { SnackService } from '../../../shared/snack.service';
import { AuthService } from 'projects/ngx-fire/auth/src/public-api';

@Component({
  selector: 'app-signup-providers',
  templateUrl: './signup-providers.component.html',
  styleUrls: ['./signup-providers.component.css']
})
export class SignupProvidersComponent {

  constructor(private snack: SnackService,
    private auth: AuthService) { }

  google() {
    this.signInWith(AuthProvider.Google)
  }

  facebook() {
    this.signInWith(AuthProvider.Facebook)
  }

  private async signInWith(provider: AuthProvider) {
    try {
      await this.auth.signInWith(provider)
      this.snack.open("Sign in success.")
    } catch(err) {
      this.snack.open(err.message)
    }
  }

}
