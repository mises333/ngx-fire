import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../../../ngx-fire/auth/src/lib/auth.service';
import { Router } from '@angular/router';
import { AuthProvider } from 'projects/ngx-fire/auth/src/lib/auth.interfaces';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {

  constructor() { }

}
