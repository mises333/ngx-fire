import { Component } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'projects/ngx-fire/auth/src/lib/auth.service';
import { SnackService } from '../shared/snack.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  isHandset$: Observable<boolean> = this.breakPointObserver.observe(['(max-width: 750px)'])
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakPointObserver: BreakpointObserver, public auth: AuthService, private snack: SnackService) {}

  async logOut(){
    try {
      await this.auth.logOut()
      this.snack.open('Log out success.')
    } catch(err) {
      this.snack.open(err.message)
    }
  }

}
