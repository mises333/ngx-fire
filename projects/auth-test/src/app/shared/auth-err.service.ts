import { Injectable } from '@angular/core';
import { AuthErrorInterface, NgxFireAuthConfig } from '../../../../ngx-fire/auth/src/lib/auth.interfaces';
import {
    SignInAnonyErrorCode, SignInEmailErrorCode, FetchSignInMethodsErrorCode,
    CanDeleteAccountErrorCode, DeleteAccountErrorCode, SendResetEmailPasswordErrorCode,
    CreateUserEmailPasswordErrorCode, SignInWithPopupErrorCode, SignInWithRedirectErrorCode,
    RedirectResultErrorCode, EmailVerifyErrorCode, AuthErrorServiceCodes
} from '../../../../ngx-fire/auth/src/lib/auth-error.enum';

export class ErrObject {
    constructor(public code: string, public message: string) { }
}

@Injectable()
export class AuthErrService implements AuthErrorInterface {

    constructor(private config: NgxFireAuthConfig) { }

    // Remember about break in switch cases when overwritten service
    signInAnony(err: any): void {
        switch (err.code) {
            case SignInAnonyErrorCode.operationNotAllowed:
                throw err
            default:
                throw err
        }
    }

    signInEmail(err: any): void {
        switch (err.code) {
            case SignInEmailErrorCode.invalidEmail:
                throw err
            case SignInEmailErrorCode.userDisabled:
                throw err
            case SignInEmailErrorCode.userNotFound:
                throw err
            case SignInEmailErrorCode.wrongPassword:
                throw err
            default:
                throw err
        }
    }

    fetchSignInMethods(err: any): void {
        switch (err.code) {
            case FetchSignInMethodsErrorCode.invalidEmail:
                throw err
            default:
                throw err
        }
    }

    canDeleteAccount(err: any): void {
        switch (err.code) {
            case CanDeleteAccountErrorCode.ngxFireCanDeleteAccount:
                throw err
            default:
                throw err
        }
    }

    deleteAccount(err: any): void {
        switch (err.code) {
            case DeleteAccountErrorCode.requiresRecentLogin:
                throw err
            default:
                throw err
        }
    }

    sendResetEmailPassword(err: any): void {
        switch (err.code) {
            case SendResetEmailPasswordErrorCode.invalidEmail:
                throw err
            case SendResetEmailPasswordErrorCode.missingAndroidPkgName:
                throw err
            case SendResetEmailPasswordErrorCode.missingContinueUri:
                throw err
            case SendResetEmailPasswordErrorCode.missingIosBundleId:
                throw err
            case SendResetEmailPasswordErrorCode.invalidContinueUri:
                throw err
            case SendResetEmailPasswordErrorCode.unauthorizedContinueUri:
                throw err
            case SendResetEmailPasswordErrorCode.userNotFound:
                throw err
            default:
                throw err
        }
    }

    createUserEmailPassword(err: any): void {
        switch (err.code) {
            case CreateUserEmailPasswordErrorCode.emailAlreadyInUse:
                throw err
            case CreateUserEmailPasswordErrorCode.invalidEmail:
                throw err
            case CreateUserEmailPasswordErrorCode.operationNotAllowed:
                throw err
            case CreateUserEmailPasswordErrorCode.weakPassword:
                throw err
            default:
                throw err
        }
    }

    signInWitchPopup(err: any): void {
        switch (err.code) {
            case SignInWithPopupErrorCode.accountExistsWithDifferentCredential:
                throw err
            case SignInWithPopupErrorCode.authDomainConfigRequired:
                throw err
            case SignInWithPopupErrorCode.cancelledPopupRequest:
                throw err
            case SignInWithPopupErrorCode.operationNotAllowed:
                throw err
            case SignInWithPopupErrorCode.operationNotSupportedInThisEnvironment:
                throw err
            case SignInWithPopupErrorCode.popupBlocked:
                throw err
            case SignInWithPopupErrorCode.popupClosedByUser:
                throw err
            case SignInWithPopupErrorCode.unauthorizedDomain:
                throw err
            default:
                throw err
        }
    }

    signInWitchRedirect(err: any): void {
        switch (err.code) {
            case SignInWithRedirectErrorCode.authDomainConfigRequired:
                throw err
            case SignInWithRedirectErrorCode.operationNotSupportedInThisEnvironment:
                throw err
            case SignInWithRedirectErrorCode.unauthorizedDomain:
                throw err
            default:
                throw err
        }
    }

    redirectResult(err: any): void {
        switch (err.code) {
            case RedirectResultErrorCode.accountExistsWithDifferentCredential:
                throw err
            case RedirectResultErrorCode.authDomainConfigRequired:
                throw err
            case RedirectResultErrorCode.credentialAlreadyInUse:
                throw err
            case RedirectResultErrorCode.emailAlreadyInUse:
                throw err
            case RedirectResultErrorCode.operationNotAllowed:
                throw err
            case RedirectResultErrorCode.operationNotSupportedInThisEnvironment:
                throw err
            case RedirectResultErrorCode.timeout:
                throw err
            default:
                throw err
        }
    }

    emailVerify(err: any): void {
        switch (err.code) {
            case EmailVerifyErrorCode.invalidContinueUri:
                throw err
            case EmailVerifyErrorCode.missingAndroidPkgName:
                throw err
            case EmailVerifyErrorCode.missingContinueUri:
                throw err
            case EmailVerifyErrorCode.missingIosBundleId:
                throw err
            case EmailVerifyErrorCode.unauthorizedContinueUri:
                throw err
            default:
                throw err
        }
    }

    handleErrors(code: string, err: any): void {
        if (err.code && this.config.enableAuthErrorService) {
            switch (code) {
                case AuthErrorServiceCodes.canDeleteAccount:
                    this.canDeleteAccount(err)
                    break
                case AuthErrorServiceCodes.createUserEmailPassword:
                    this.createUserEmailPassword(err)
                    break
                case AuthErrorServiceCodes.deleteAccount:
                    this.deleteAccount(err)
                    break
                case AuthErrorServiceCodes.emailVerify:
                    this.emailVerify(err)
                    break
                case AuthErrorServiceCodes.fetchSignInMethods:
                    this.fetchSignInMethods(err)
                    break
                case AuthErrorServiceCodes.redirectResult:
                    this.redirectResult(err)
                    break
                case AuthErrorServiceCodes.sendResetEmailPassword:
                    this.sendResetEmailPassword(err)
                    break
                case AuthErrorServiceCodes.signInAnony:
                    this.signInAnony(err)
                    break
                case AuthErrorServiceCodes.signInEmail:
                    this.signInEmail(err)
                    break
                case AuthErrorServiceCodes.signInWitchPopup:
                    this.signInWitchPopup(err)
                    break
                case AuthErrorServiceCodes.signInWitchRedirect:
                    this.signInWitchRedirect(err)
                    break
                default:
                    throw err
            }
        }
        throw err
    }

}

