import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from "rxjs";
import { AngularFirestore } from "@angular/fire/firestore";

export class FirestoreTranslateLoader implements TranslateLoader {
    constructor(private db: AngularFirestore) { }
    getTranslation(lang: string, prefix: string = 'lang'): Observable<any> {
        return this.db.collection('' + prefix).doc('' + lang).valueChanges() as Observable<any>;
    }
}