import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable()
export class SnackService {

    constructor(private snackBar: MatSnackBar) { }

    public config(panelClasses: string) {
        let config = new MatSnackBarConfig()
        config.panelClass = [panelClasses]
        return config
    }

    open(message: string, config?: MatSnackBarConfig<any>, action?: string) {
        if(config) {
            config.duration = 5000
        }
        this.snackBar.open(message, action ? action : '', config ? config : { duration: 5000 })
    }
}