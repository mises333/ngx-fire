import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirestoreTranslateLoader } from "./firestore-translate-loader";

export function AngularFireFactory(db: AngularFirestore) {
  return new FirestoreTranslateLoader(db);
}

const transLoader = {
  loader: {
    provide: TranslateLoader,
    useFactory: AngularFireFactory,
    deps: [AngularFirestore]
  }
}

@NgModule({
  declarations: [],
  imports: [
    TranslateModule.forRoot(transLoader),
  ],
  exports: [
    TranslateModule,
  ]
})
export class TranslModule { }