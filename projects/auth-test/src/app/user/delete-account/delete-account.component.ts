import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from 'projects/ngx-fire/auth/src/lib/auth.service';
import { SnackService } from '../../shared/snack.service';

@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.css']
})
export class DeleteAccountComponent implements OnInit {

  hide: true;
  @Input() disabled: boolean;

  password = new FormControl('', [Validators.required, Validators.minLength(6)])
  formIsValid: boolean;

  constructor(public dialogRef: MatDialogRef<DeleteAccountComponent>, public auth: AuthService, private snack: SnackService) { }

  ngOnInit() {
  }

  async deleteAccount() {
    if (!this.password.invalid) {
      try {
        await this.auth.deleteAccount(this.password.value)
        this.dialogRef.close()
        this.snack.open("Delete success.")
      } catch(err) {
        if (err.code) this.snack.open(err.message)
        else throw err
      }
    }
  }

  onNoClick() {
    this.dialogRef.close()
  }

  getErrorPassword() {
    return this.password.hasError('required') ? 'You must enter a password.' :
      this.password.hasError('minlength') ? 'Password should have at least 6 haracters' : '';
  }

}
