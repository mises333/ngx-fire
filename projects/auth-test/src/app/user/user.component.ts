import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../ngx-fire/auth/src/lib/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { SnackService } from '../shared/snack.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(public auth: AuthService, private dialog: MatDialog, private snack: SnackService) { }

  ngOnInit() {
  }

  async openDeleteAccountDialog() {
    try {
      await this.auth.canDeleteAccount()
      this.dialog.open(DeleteAccountComponent,
        {width: '500px'} );
    } catch(err) {
      this.snack.open(err.message)
    }
  }

  async emailReset() {
    try {
      await this.auth.resetPassword()
      this.snack.open('Email sended.')
    } catch(err) {
      if(err.code) this.snack.open(err.message)
      else throw err
    }
  }

}
