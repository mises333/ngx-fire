/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/operation-not-allowed</dt>
     * <dd>Thrown if anonymous accounts are not enabled. Enable anonymous accounts
     *     in the Firebase Console, under the Auth tab.</dd>
     * </dl>
*/
export enum SignInAnonyErrorCode {
    operationNotAllowed = 'auth/operation-not-allowed'
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/invalid-email</dt>
     * <dd>Thrown if the email address is not valid.</dd>
     * <dt>auth/user-disabled</dt>
     * <dd>Thrown if the user corresponding to the given email has been
     *     disabled.</dd>
     * <dt>auth/user-not-found</dt>
     * <dd>Thrown if there is no user corresponding to the given email.</dd>
     * <dt>auth/wrong-password</dt>
     * <dd>Thrown if the password is invalid for the given email, or the account
     *     corresponding to the email does not have a password set.</dd>
     * </dl>
*/
export enum SignInEmailErrorCode {
    invalidEmail = 'auth/invalid-email',
    userDisabled = 'auth/user-disabled',
    userNotFound = 'auth/user-not-found',
    wrongPassword = 'auth/wrong-password',
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/invalid-email</dt>
     * <dd>Thrown if the email address is not valid.</dd>
     * </dl>
 */
export enum FetchSignInMethodsErrorCode {
    invalidEmail = 'auth/invalid-email',
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>ngx-fire/can-delete-account</dt>
     * <dd>Thrown if User have no provider as password so he dont have password.</dd>
*/
export enum CanDeleteAccountErrorCode {
    ngxFireCanDeleteAccount = 'ngx-fire/can-delete-account'
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/requires-recent-login</dt>
     * <dd>Thrown if the user's last sign-in time does not meet the security
     *     threshold. Use {@link firebase.User.reauthenticateWithCredential} to
     *     resolve. This does not apply if the user is anonymous.</dd>
     * </dl>
*/
export enum DeleteAccountErrorCode {
    requiresRecentLogin = 'auth/requires-recent-login',
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/invalid-email</dt>
     * <dd>Thrown if the email address is not valid.</dd>
     * <dt>auth/missing-android-pkg-name</dt>
     * <dd>An Android package name must be provided if the Android app is required
     *     to be installed.</dd>
     * <dt>auth/missing-continue-uri</dt>
     * <dd>A continue URL must be provided in the request.</dd>
     * <dt>auth/missing-ios-bundle-id</dt>
     * <dd>An iOS Bundle ID must be provided if an App Store ID is provided.</dd>
     * <dt>auth/invalid-continue-uri</dt>
     * <dd>The continue URL provided in the request is invalid.</dd>
     * <dt>auth/unauthorized-continue-uri</dt>
     * <dd>The domain of the continue URL is not whitelisted. Whitelist
     *     the domain in the Firebase console.</dd>
     * <dt>auth/user-not-found</dt>
     * <dd>Thrown if there is no user corresponding to the email address.</dd>
     * </dl>
*/
export enum SendResetEmailPasswordErrorCode {
    invalidEmail = 'auth/invalid-email',
    missingAndroidPkgName = 'auth/missing-android-pkg-name',
    missingContinueUri = 'auth/missing-continue-uri',
    missingIosBundleId = 'auth/missing-ios-bundle-id',
    invalidContinueUri = 'auth/invalid-continue-uri',
    unauthorizedContinueUri = 'auth/unauthorized-continue-uri',
    userNotFound = 'auth/user-not-found',
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/email-already-in-use</dt>
     * <dd>Thrown if there already exists an account with the given email
     *     address.</dd>
     * <dt>auth/invalid-email</dt>
     * <dd>Thrown if the email address is not valid.</dd>
     * <dt>auth/operation-not-allowed</dt>
     * <dd>Thrown if email/password accounts are not enabled. Enable email/password
     *     accounts in the Firebase Console, under the Auth tab.</dd>
     * <dt>auth/weak-password</dt>
     * <dd>Thrown if the password is not strong enough.</dd>
     * </dl>
*/
export enum CreateUserEmailPasswordErrorCode {
    emailAlreadyInUse = 'auth/email-already-in-use',
    invalidEmail = 'auth/invalid-email',
    operationNotAllowed = 'auth/operation-not-allowed',
    weakPassword = 'auth/weak-password',
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/account-exists-with-different-credential</dt>
     * <dd>Thrown if there already exists an account with the email address
     *     asserted by the credential. Resolve this by calling
     *     {@link firebase.auth.Auth.fetchProvidersForEmail} with the error.email
     *     and then asking the user to sign in using one of the returned providers.
     *     Once the user is signed in, the original credential retrieved from the
     *     error.credential can be linked to the user with
     *     {@link firebase.User.linkWithCredential} to prevent the user from signing
     *     in again to the original provider via popup or redirect. If you are using
     *     redirects for sign in, save the credential in session storage and then
     *     retrieve on redirect and repopulate the credential using for example
     *     {@link firebase.auth.GoogleAuthProvider.credential} depending on the
     *     credential provider id and complete the link.</dd>
     * <dt>auth/auth-domain-config-required</dt>
     * <dd>Thrown if authDomain configuration is not provided when calling
     *     firebase.initializeApp(). Check Firebase Console for instructions on
     *     determining and passing that field.</dd>
     * <dt>auth/cancelled-popup-request</dt>
     * <dd>Thrown if successive popup operations are triggered. Only one popup
     *     request is allowed at one time. All the popups would fail with this error
     *     except for the last one.</dd>
     * <dt>auth/operation-not-allowed</dt>
     * <dd>Thrown if the type of account corresponding to the credential
     *     is not enabled. Enable the account type in the Firebase Console, under
     *     the Auth tab.</dd>
     * <dt>auth/operation-not-supported-in-this-environment</dt>
     * <dd>Thrown if this operation is not supported in the environment your
     *     application is running on. "location.protocol" must be http or https.
     *     </dd>
     * <dt>auth/popup-blocked</dt>
     * <dd>Thrown if the popup was blocked by the browser, typically when this
     *     operation is triggered outside of a click handler.</dd>
     * <dt>auth/popup-closed-by-user</dt>
     * <dd>Thrown if the popup window is closed by the user without completing the
     *     sign in to the provider.</dd>
     * <dt>auth/unauthorized-domain</dt>
     * <dd>Thrown if the app domain is not authorized for OAuth operations for your
     *     Firebase project. Edit the list of authorized domains from the Firebase
     *     console.</dd>
     * </dl>
     * */
export enum SignInWithPopupErrorCode {
    accountExistsWithDifferentCredential = 'auth/account-exists-with-different-credential',
    authDomainConfigRequired = 'auth/auth-domain-config-required',
    cancelledPopupRequest = 'auth/cancelled-popup-request',
    operationNotAllowed = 'auth/operation-not-allowed',
    operationNotSupportedInThisEnvironment = 'auth/operation-not-supported-in-this-environment',
    popupBlocked = 'auth/popup-blocked',
    popupClosedByUser = 'auth/popup-closed-by-user',
    unauthorizedDomain = 'auth/unauthorized-domain'
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/auth-domain-config-required</dt>
     * <dd>Thrown if authDomain configuration is not provided when calling
     *     firebase.initializeApp(). Check Firebase Console for instructions on
     *     determining and passing that field.</dd>
     * <dt>auth/operation-not-supported-in-this-environment</dt>
     * <dd>Thrown if this operation is not supported in the environment your
     *     application is running on. "location.protocol" must be http or https.
     *     </dd>
     * <dt>auth/unauthorized-domain</dt>
     * <dd>Thrown if the app domain is not authorized for OAuth operations for your
     *     Firebase project. Edit the list of authorized domains from the Firebase
     *     console.</dd>
     * </dl>
*/
export enum SignInWithRedirectErrorCode {
    authDomainConfigRequired = 'auth/auth-domain-config-required',
    operationNotSupportedInThisEnvironment = 'auth/operation-not-supported-in-this-environment',
    unauthorizedDomain = 'auth/unauthorized-domain'
}

/** <h4>Error Codes</h4>
     * <dl>
     * <dt>auth/account-exists-with-different-credential</dt>
     * <dd>Thrown if there already exists an account with the email address
     *     asserted by the credential. Resolve this by calling
     *     {@link firebase.auth.Auth.fetchProvidersForEmail} with the error.email
     *     and then asking the user to sign in using one of the returned providers.
     *     Once the user is signed in, the original credential retrieved from the
     *     error.credential can be linked to the user with
     *     {@link firebase.User.linkWithCredential} to prevent the user from signing
     *     in again to the original provider via popup or redirect. If you are using
     *     redirects for sign in, save the credential in session storage and then
     *     retrieve on redirect and repopulate the credential using for example
     *     {@link firebase.auth.GoogleAuthProvider.credential} depending on the
     *     credential provider id and complete the link.</dd>
     * <dt>auth/auth-domain-config-required</dt>
     * <dd>Thrown if authDomain configuration is not provided when calling
     *     firebase.initializeApp(). Check Firebase Console for instructions on
     *     determining and passing that field.</dd>
     * <dt>auth/credential-already-in-use</dt>
     * <dd>Thrown if the account corresponding to the credential already exists
     *     among your users, or is already linked to a Firebase User.
     *     For example, this error could be thrown if you are upgrading an anonymous
     *     user to a Google user by linking a Google credential to it and the Google
     *     credential used is already associated with an existing Firebase Google
     *     user.
     *     An <code>error.email</code> and <code>error.credential</code>
     *     ({@link firebase.auth.AuthCredential}) fields are also provided. You can
     *     recover from this error by signing in with that credential directly via
     *     {@link firebase.auth.Auth.signInWithCredential}.</dd>
     * <dt>auth/email-already-in-use</dt>
     * <dd>Thrown if the email corresponding to the credential already exists
     *     among your users. When thrown while linking a credential to an existing
     *     user, an <code>error.email</code> and <code>error.credential</code>
     *     ({@link firebase.auth.AuthCredential}) fields are also provided.
     *     You have to link the credential to the existing user with that email if
     *     you wish to continue signing in with that credential. To do so, call
     *     {@link firebase.auth.Auth.fetchProvidersForEmail}, sign in to
     *     <code>error.email</code> via one of the providers returned and then
     *     {@link firebase.User.linkWithCredential} the original credential to that
     *     newly signed in user.</dd>
     * <dt>auth/operation-not-allowed</dt>
     * <dd>Thrown if the type of account corresponding to the credential
     *     is not enabled. Enable the account type in the Firebase Console, under
     *     the Auth tab.</dd>
     * <dt>auth/operation-not-supported-in-this-environment</dt>
     * <dd>Thrown if this operation is not supported in the environment your
     *     application is running on. "location.protocol" must be http or https.
     *     </dd>
     * <dt>auth/timeout</dt>
     * <dd>Thrown typically if the app domain is not authorized for OAuth operations
     *     for your Firebase project. Edit the list of authorized domains from the
     *     Firebase console.</dd>
     * </dl>
*/
export enum RedirectResultErrorCode {
    accountExistsWithDifferentCredential = 'auth/account-exists-with-different-credential',
    authDomainConfigRequired = 'auth/auth-domain-config-required',
    credentialAlreadyInUse = 'auth/credential-already-in-use',
    emailAlreadyInUse = 'auth/email-already-in-use',
    operationNotAllowed = 'auth/operation-not-allowed',
    operationNotSupportedInThisEnvironment = 'auth/operation-not-supported-in-this-environment',
    timeout = 'auth/timeout'
}

/**<h4>Error Codes</h4>
     * <dl>
     * <dt>auth/missing-android-pkg-name</dt>
     * <dd>An Android package name must be provided if the Android app is required
     *     to be installed.</dd>
     * <dt>auth/missing-continue-uri</dt>
     * <dd>A continue URL must be provided in the request.</dd>
     * <dt>auth/missing-ios-bundle-id</dt>
     * <dd>An iOS bundle ID must be provided if an App Store ID is provided.</dd>
     * <dt>auth/invalid-continue-uri</dt>
     * <dd>The continue URL provided in the request is invalid.</dd>
     * <dt>auth/unauthorized-continue-uri</dt>
     * <dd>The domain of the continue URL is not whitelisted. Whitelist
     *     the domain in the Firebase console.</dd>
     * </dl>
*/
export enum EmailVerifyErrorCode {
    missingAndroidPkgName = 'auth/missing-android-pkg-name',
    missingContinueUri = 'auth/missing-continue-uri',
    missingIosBundleId = 'auth/missing-ios-bundle-id',
    invalidContinueUri = 'auth/invalid-continue-uri',
    unauthorizedContinueUri = 'auth/unauthorized-continue-uri'
}

export enum AuthErrorServiceCodes {
    signInAnony = 'sign-in-anony',
    signInEmail = 'sign-in-email',
    fetchSignInMethods = 'fetch-sign-in-methods',
    canDeleteAccount = 'can-delete-account',
    deleteAccount = 'delete-account',
    sendResetEmailPassword = 'send-reset-email-password',
    createUserEmailPassword = 'create-user-email-password',
    signInWitchPopup = 'sign-in-witch-popup',
    signInWitchRedirect = 'sign-in-witch-redirect',
    redirectResult = 'redirect-result',
    emailVerify = 'email-verify',
}