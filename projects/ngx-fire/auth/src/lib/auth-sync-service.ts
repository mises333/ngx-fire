import { Injectable } from '@angular/core';
import { User } from './auth.interfaces';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class AuthSyncService {

    constructor(private db: AngularFirestore) { }

    public updateUserData(user: firebase.User, accDeleted?: boolean) {
        const userRef: AngularFirestoreDocument<User> = this.db.doc('users/' + user.uid)
        const data: User = {
            createdBy: 'user',
            uid: user.uid,
            email: user.email,
            emailVerified: user.emailVerified,
            displayName: user.displayName,
            phoneNumber: user.phoneNumber,
            photoURL: user.photoURL,
            accDeleted: accDeleted ? true : false,
            providerId: user.providerData? user.providerData[0].providerId : user.providerId,
            isAnonymous: user.isAnonymous,
        }
        return userRef.set(data, { merge: true })
    }

    public setUserData(user: User) {
        const userRef: AngularFirestoreDocument<User> = this.db.doc('users/' + user.uid)
        return userRef.set(user, { merge: true })
    }

    public getUserData(uid: string) {
        return this.db.doc('users/' + uid).valueChanges()
    }

}