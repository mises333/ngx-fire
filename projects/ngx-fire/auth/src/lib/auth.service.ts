import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthProvider, IdCredentials, User, NgxFireAuthConfig } from './auth.interfaces';
import * as firebase from 'firebase';
import UserCredential = firebase.auth.UserCredential;
import { AuthErrorService, ErrObject } from './auth-error.service';
import { AuthSyncService } from './auth-sync-service';
import { AuthErrorServiceCodes } from './auth-error.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<User | firebase.User>
  isLoading: boolean

  constructor(public afAuth: AngularFireAuth,
    private error: AuthErrorService,
    private syncService: AuthSyncService,
    private config: NgxFireAuthConfig) {
    this.syncUser()
  }

  public async syncUser() {
    try {
      this.isLoading = true
      this.user$ = this.afAuth.authState.pipe(
        switchMap(user => {
          if (user) {
            if (this.config.enableFirestoreSync) {
              this.syncService.updateUserData(user)
              return this.syncService.getUserData(user.uid)
            } else {
              return of(user)
            }
          } else {
            return of(null)
          }
        })
      )
    } finally {
      this.isLoading = false
    }
  }

  public async sendEmailVerification(langCode?: string) {
    try {
      this.isLoading = true
      this.afAuth.auth.languageCode = langCode ? langCode : 'en'
      await this.afAuth.auth.currentUser.sendEmailVerification()
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.emailVerify, err))
    } finally {
      this.isLoading = false
    }
  }

  public async signInWith(provider: AuthProvider, credentials?: IdCredentials): Promise<void> {
    try {
      this.isLoading = true
      let signInResult: UserCredential;
      switch (provider) {
        case AuthProvider.EmailAndPassword:
          signInResult = await this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
            .catch(err => this.error.handleErrors(AuthErrorServiceCodes.signInEmail, err)) as UserCredential
          break
        case AuthProvider.ANONYMOUS:
          signInResult = await this.afAuth.auth.signInAnonymously()
            .catch(err => this.error.handleErrors(AuthErrorServiceCodes.signInAnony, err)) as UserCredential
          break
        case AuthProvider.Google:
          signInResult = await this.signIn(new firebase.auth.GoogleAuthProvider) as UserCredential
          break
        case AuthProvider.Facebook:
          signInResult = await this.signIn(new firebase.auth.FacebookAuthProvider) as UserCredential
          break
        case AuthProvider.GitHub:
          signInResult = await this.signIn(new firebase.auth.GithubAuthProvider) as UserCredential
          break
        case AuthProvider.Twitter:
          signInResult = await this.signIn(new firebase.auth.TwitterAuthProvider) as UserCredential
          break
        default:
          throw new Error(provider + ' is not avaible as authentication provider') //to do translate
      }
      this.config.enableFirestoreSync ? this.syncService.updateUserData(signInResult.user) : null
    } finally {
      this.isLoading = false
    }
  }

  private async signIn(provider: firebase.auth.AuthProvider): Promise<UserCredential> {
    const windowWidth = window.innerWidth
    if (windowWidth > 1025) {
      return await this.afAuth.auth.signInWithPopup(provider)
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.signInWitchPopup, err)) as UserCredential
    } else {
      await this.afAuth.auth.signInWithRedirect(provider)
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.signInWitchRedirect, err))
      return await this.afAuth.auth.getRedirectResult()
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.redirectResult, err)) as UserCredential
    }
  }

  public async signUpEmail(cridentials: IdCredentials) {
    try {
      this.isLoading = true
      const createResult = await this.afAuth.auth.createUserWithEmailAndPassword(cridentials.email, cridentials.password)
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.createUserEmailPassword, err)) as UserCredential
      this.config.enableFirestoreSync ? this.syncService.updateUserData(createResult.user) : null
    } finally {
      this.isLoading = false
    }
  }

  async logOut(): Promise<void> {
    try {
      this.isLoading = true
      await this.afAuth.auth.signOut()
    } finally {
      this.isLoading = false
    }
  }

  async canDeleteAccount() {
    try {
      this.isLoading = true
      const user = this.afAuth.auth.currentUser
      let hassEmail: boolean = false
      const providers = await this.afAuth.auth.fetchSignInMethodsForEmail(user.email)
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.fetchSignInMethods, err)) as string[]
      providers.forEach(value => (value == 'password') ? hassEmail = true : null)
      if (!hassEmail) {
        this.error.handleErrors(AuthErrorServiceCodes.canDeleteAccount, new ErrObject('ngx-fire/can-delete-account', 'You need to set up password to your account.'))
      }
    } finally {
      this.isLoading = false
    }
  }

  async deleteAccount(password: string): Promise<any> {
    try {
      this.isLoading = true
      const user = this.afAuth.auth.currentUser
      await this.afAuth.auth.signInWithEmailAndPassword(user.email, password)
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.signInEmail, err))
      await this.syncService.getUserData(user.uid) ? this.syncService.updateUserData(user, true) : null
      await user.delete().catch(err => {
        this.syncService.getUserData(user.uid) ? this.syncService.updateUserData(user, false) : null
        this.error.handleErrors(AuthErrorServiceCodes.deleteAccount, err)
      })
    } finally {
      this.isLoading = false
    }
  }

  async resetPassword() {
    try {
      this.isLoading = true
      const user = this.afAuth.auth.currentUser
      await this.afAuth.auth.sendPasswordResetEmail(user.email)
        .catch(err => this.error.handleErrors(AuthErrorServiceCodes.sendResetEmailPassword, err))
    } finally {
      this.isLoading = false
    }
  }

}
