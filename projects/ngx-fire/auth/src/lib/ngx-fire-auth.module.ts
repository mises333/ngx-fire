import { NgModule, ModuleWithProviders } from '@angular/core';
import { AuthService } from './auth.service';
import { AuthSyncService } from './auth-sync-service';
import { AuthErrorService } from './auth-error.service';
import { NgxFireAuthConfig } from './auth.interfaces';

@NgModule({
  declarations: [],
  imports: []
})
export class NgxFireAuthModule {
  static forRoot(config?: NgxFireAuthConfig): ModuleWithProviders {
    return {
      ngModule: NgxFireAuthModule,
      providers: [
        {
          provide: NgxFireAuthConfig,
          useValue: config ? config : { enableFirestoreSync: true, enableAuthErrorService: false }
        },
        AuthErrorService, AuthSyncService, AuthService
      ]
    }
  }
}
