/*
 * Public API Surface of auth
 */

export * from './lib/auth-error.enum';
export * from './lib/auth-error.service';
export * from './lib/auth-sync-service';
export * from './lib/auth.interfaces';
export * from './lib/auth.service';
export * from './lib/ngx-fire-auth.module';
